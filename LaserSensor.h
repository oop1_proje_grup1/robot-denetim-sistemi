#pragma once
#include"PioneerRobotAPI.h"

/*-> File : SonarSensor.h
-> Author : Mehmet Yumrukaya
-> Date : 20.12.2018
*/

class LaserSensor
{
private:
	float ranges[181];

	PioneerRobotAPI* robotAPI;

public:
	LaserSensor();

	~LaserSensor();

	float getRange(int index);
	void updateSensor(float ranges[]);
	float getMax();
	float getMin();
	float operator[] (int i);
};


