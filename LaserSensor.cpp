#include "LaserSensor.h"



LaserSensor::LaserSensor()
{
}


LaserSensor::~LaserSensor()
{
}

float LaserSensor::getRange(int index) {

	return ranges[index];
}

float LaserSensor::operator[](int i) {

	return ranges[i];
}

float LaserSensor::getMax() {

	float max = ranges[0];

	for (int i = 0; i < 181; i++) {
		if (max < ranges[i])
			max = ranges[i];
	}
	return max;
}

float LaserSensor::getMin() {

	float min = ranges[0];

	for (int i = 0; i < 181; i++) {
		if (min < ranges[i])
			min = ranges[i];
	}
	return min;
}

void LaserSensor::updateSensor(float ranges[]) {

	for (int i = 0; i < 181; i++) {
		this->ranges[i] = ranges[i];
	}
}
