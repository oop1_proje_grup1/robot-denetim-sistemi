#pragma once

/**
*\file RobotControl.h
*\author Alper DEMIREL
*\date 20.12.2018
*\brief RobotControl sinifi ve fonksiyonlarinin prototipleri
*/

#include"Pose.h"
#include"PioneerRobotAPI.h"
#include <Windows.h>
#include <conio.h>
#include <cmath>

using namespace std;

/**
*\brief Robotun kontrolunde kullanilan siniftir
*/
class RobotControl
{
private:

	/**
	* \brief Robotun pozisyon bilgilerinin oldugu nesneyi gosterir
	*/
	Pose position;

	/**
	* \brief Robotun durumunu gosterir
	*/
	int state;

	/**
	* \brief RobotControl nesnesinin baglandigi PioneerRobotAPI nesnesini gosterir
	*/
	PioneerRobotAPI* robotAPI;

public:
	
	/**
	* \brief RobotControl sinifinin yapici fonksiyonu
	*/
	RobotControl(Pose position,PioneerRobotAPI* robotAPI );

	/**
	* \brief RobotControl sinifinin varsayilan yikici fonksiyonu
	*/
	~RobotControl();

	/**
	* \brief Robotun 90 derece sola donmesini saglar
	*/
	void turnLeft();

	/**
	* \brief Robotun 90 derece saga donmesini saglar
	*/
	void turnRight();

	/**
	* \brief Robotun verilen hizda ileri gitmesini saglar
	* \param speed : Robotun gitmesi istenilen hiz
	*/
	void forward(float speed);

	/**
	* \brief Robotun konumunu ve yon bilgilerini yazdirir
	*/
	void print()const;

	/**
	* \brief Robotun verilen hizda geri gitmesini saglar
	* \param speed : Robotun gitmesi istenilen hiz
	*/
	void backward(float speed);
	
	/**
	*\brief Robotun pozisyonuna eri�mek i�in �a�r�lan fonksiyon
	*\return Robotun pozisyonu
	*/
	Pose getPose();

	/**
	*\brief Robotun pozisyonunu degistiren fonksiyon
	*\param Pose: Yeni pozisyonun degeri
	*/
	void setPose(Pose position);

	/**
	*\brief Robotun d�nmesini durduran fonksiyon
	*/
	void stopTurn();

	/**
	*\brief Robotun y�r�mesini durduran fonksiyon
	*/
	void stopMove();
};

