#pragma once

/**
*\file Pose.h
*\author Alper DEMIREL
*\date 20.12.2018
*\brief Pose sinifi ve fonksiyonlarinin prototipleri
*/

/**
*\brief Robotun pozisyon degerleri ve islemleri icin bir sinif
*/
class Pose
{
private:

	/**
	*\brief Robotun pozisyonunun x koordinati
	*/
	float x;

	/**
	*\brief Robotun pozisyonunun y koordinati
	*/
	float y;

	/**
	*\brief Robotun pozisyonunun acisi
	*/
	float th;

public:
	
	/**
	*\brief Varsayilan yapici fonksiyon
	*/
	Pose();
	
	/**
	*\brief Varsayilan yikici fonksiyon
	*/
	~Pose();

	/**
	*\brief Robotun pozisyonunun x koordinatina erismek icin bir fonksiyon
	*\return Robotun pozisyonunun x koordinati
	*/
	float getX()const;

	/**
	*\brief Robotun pozisyonunun x koordinatini degistiren fonksiyon
	*\param x: Yeni x koordinatinin degeri
	*/
	void setX(float  _x);

	/**
	*\brief Robotun pozisyonunun y koordinatina erismek icin bir fonksiyon
	*\return Robotun pozisyonunun y koordinati
	*/
	float getY()const;

	/**
	*\brief Robotun pozisyonunun y koordinatini degistiren fonksiyon
	*\param y: Yeni y koordinatinin degeri
	*/
	void setY(float _y);

	/**
	*\brief Robotun pozisyonunun acisina erismek icin bir fonksiyon
	*\return Robotun pozisyonunun acisi
	*/
	float getTh()const;

	/**
	*\brief Robotun pozisyonunun acisini degistiren fonksiyon
	*\param th: Yeni acinin degeri
	*/
	void setTh(float _th);

	/**
	*\brief Iki pozisyon esitli�i icin kullanilacak operat�r�n fonksiyonu
	*\param Pose: Karsilastiracagimiz di�er pozisyon
	*\return Pozisyonlar esitse true, degilse false
	*/
	bool operator==(const Pose& other);

	/**
	*\brief Iki pozisyonu toplamak i�in kullanilacak operat�r�n fonksiyonu
	*\param Pose: Toplayacagimiz diger pozisyon
	*\return Pozisyonlarin toplami
	*/
	Pose operator+(const Pose& other);

	/**
	*\brief Iki pozisyonu cikarmak icin kullanilacak operat�r�n fonksiyonu
	*\param Pose: Cikaracagimiz diger pozisyon
	*\return Pozisyonlarin cikarimi
	*/
	Pose operator-(const Pose& other);

	/**
	*\brief pose+=poseother i�lemini yapmak icin kullanilacak operat�r�n fonksiyonu
	*\param Pose: Islem yapacagimiz diger pozisyon
	*\return pose+=poseother i�leminin sonucu
	*/
	Pose& operator+=(const Pose& other);

	/**
	*\brief pose-=poseother i�lemini yapmak icin kullanilacak operat�r�n fonksiyonu
	*\param Pose: Islem yapacagimiz diger pozisyon
	*\return pose-=poseother i�leminin sonucu
	*/
	Pose& operator-=(const Pose& other);

	/**
	*\brief Iki pozisyonun hangisinin k�c�k oldugunu bulmak icin kullanilacak operat�r�n fonksiyonu
	*\param Pose: Karsilastiracagimiz di�er fonksiyon
	*\return Pozisyonlarin biri digerinden k�c�kse true, degilse false
	*/
	bool operator<(const Pose& other);

	/**
	*\brief Robotun pozisyonuna erismek icin bir fonksiyon
	*\param x: Pozisyonun x koordinati
	*\param y: Pozisyonun y koordinati
	*\param th: Pozisyonun th acisi
	*/
	void getPose(float& _x, float& _y, float& _th);

	/**
	*\brief Robotun pozisyonunu degistiren fonksiyon
	*\param x: Yeni x koordinatinin degeri
	*\param y: Yeni y koordinatinin degeri
	*\param th: Yeni acinin degeri
	*/
	void setPose(float _x, float _y, float _th);

	/**
	*\brief Pozisyonun x ve y koordinatinin mesafesini bulan fonksiyon
	*\param pos: Mesafesini bulacagimiz pozisyonun degeri
	*\return Pozisyonun mesafesi
	*/
	float findDistanceTo(Pose pos);

	/**
	*\brief Pozisyonun acisini bulan fonksiyon
	*\param pos: Acisini bulacagimiz pozisyonun degeri
	*\return Pozisyonun acisi
	*/
	float findAngleTo(Pose pos);
};

