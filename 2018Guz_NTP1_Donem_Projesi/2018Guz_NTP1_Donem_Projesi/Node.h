#pragma once

/**
*\file Node.h
*\author Alper DEMIREL
*\date 20.12.2018
*\brief Node sinifi ve fonksiyonlarinin prototipleri
*/

#include"Pose.h"

/**
*\brief Linked list olusturdugumuz sinif
*/
class Node
{

public:

	/**
	*\brief Varsayilan yapici fonksiyon
	*/
	Node();

	/**
	*\brief Varsayilan yikici fonksiyon
	*/
	~Node();

	/**
	*\brief Listenin ilerlemesini saglayan pointer
	*/
	Node* next;

	/**
	* \brief Pozisyon bilgilerinin oldugu nesneyi gosterir
	*/
	Pose pose;
};

