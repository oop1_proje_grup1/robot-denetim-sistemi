#include "RobotControl.h"

/**
*\file RobotControl.cpp
*\author Alper DEMIREL
*\date 20.12.2018
*\brief RobotControl sinifinin kodlamalari
*/

RobotControl::RobotControl(Pose position, PioneerRobotAPI * robotAPI)
{
	this->position = position;
	this->robotAPI = robotAPI;
}

RobotControl::~RobotControl()
{
}
void RobotControl::turnLeft() {
	float _th;
	robotAPI->turnRobot(PioneerRobotAPI::left);

	Sleep(1000);
	_th = abs(position.getTh());
	if (_th < 10 || (position.getTh() > 80 && position.getTh() < 100)) {
		while ((int)_th % 90 < 86) {
			Sleep(200);
			print();
			_th = abs(position.getTh());
		}
	}
	else {
		while ((int)_th % 90 > 3) {
			Sleep(200);
			print();
			_th = abs(position.getTh());
		}
	}
	robotAPI->turnRobot(PioneerRobotAPI::forward);
}

void RobotControl::turnRight() {
	float _th;
	robotAPI->turnRobot(PioneerRobotAPI::right);
	Sleep(1000);
	_th = abs(position.getTh());
	if (_th < 10 || (position.getTh() < -80 && position.getTh() > -100)) {
		while ((int)_th % 90 < 86) {
			Sleep(200);
			print();
			_th = abs(position.getTh());
		}
	}
	else {
		while ((int)_th % 90 > 3) {
			Sleep(200);
			print();
			_th = abs(position.getTh());
		}
	}
	robotAPI->turnRobot(PioneerRobotAPI::forward);
}

void RobotControl::forward(float speed)
{
	robotAPI->turnRobot(PioneerRobotAPI::forward);
	robotAPI->moveRobot(speed);
	
}

void RobotControl::print() const
{
	    cout << "MyPose is (" << position.getX() << "mm, " << position.getY() << "mm, " << position.getTh() << "�)" << endl;
	
}

void RobotControl::backward(float speed)
{
	robotAPI->turnRobot(PioneerRobotAPI::forward);
	robotAPI->moveRobot(-speed);
}

Pose RobotControl::getPose()
{
	return position;
}

void RobotControl::setPose(Pose position)
{
	this->position = position;
}

void RobotControl::stopTurn()
{

}

void RobotControl::stopMove()
{
	robotAPI->stopRobot();
}
