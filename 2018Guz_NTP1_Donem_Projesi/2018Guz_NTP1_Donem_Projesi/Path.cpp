#include "Path.h"

/**
*\file Path.cpp
*\author Alper DEMIREL
*\date 20.12.2018
*\brief Path sinifinin kodlamalari
*/

Path::Path()
{
	head = NULL;
	tail = NULL;
}


Path::~Path()
{
}

void Path::addPos(Pose pos)
{
	Node *tmp = new Node();
	tmp->pose = pos;
	tmp->next = NULL;

	if (head == NULL)
	{
		head = tmp;
		tail = tmp;
	}
	else
	{
		tail->next = tmp;
		tail = tail->next;
	}
}

void Path::print(Node* head)
{
	if (head == NULL)
	{
		cout << "NULL" << endl;
	}
	else
	{
		cout << head<< endl;
		print(head->next);
	}
}


