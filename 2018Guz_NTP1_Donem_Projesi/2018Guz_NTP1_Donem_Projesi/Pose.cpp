#include "Pose.h"

/**
*\file Pose.cpp
*\author Alper DEMIREL
*\date 20.12.2018
*\brief Pose sinifinin fonksiyon kodlamalari
*/

Pose::Pose()
{
}


Pose::~Pose()
{
}
float Pose::getX()const
{
	return x;
}

void Pose::setX(float _x)
{
	x = _x;
}

float Pose::getY()const
{
	return y;
}

void Pose::setY(float _y)
{
	y = _y;
}

float Pose::getTh()const
{
	return th;
}

void Pose::setTh(float _th)
{
	th = _th;
}

bool Pose::operator==(const Pose & other)
{
	if (x == other.x && y == other.y && th == other.th)
		return true;
	return false;
}

Pose Pose::operator+(const Pose & other)
{
	Pose pos;
	pos.x = x + other.x;
	pos.y = y + other.y;
	pos.th = th + other.th;
	return pos;
}

Pose Pose::operator-(const Pose & other)
{
	Pose pos;
	pos.x = x - other.x;
	pos.y = y - other.y;
	pos.th = th - other.th;
	return pos;
}

Pose & Pose::operator+=(const Pose & other)
{

	x = x + other.x;
	y = y + other.y;
	th = th + other.th;
	return *this;
}

Pose & Pose::operator-=(const Pose & other)
{
	x = x - other.x;
	y = y - other.y;
	th = th - other.th;
	return *this;
}

bool Pose::operator<(const Pose & other)
{
	if (x < other.x && y < other.y && th < other.th)
		return true;
	return false;
}

void Pose::getPose(float & _x, float & _y, float & _th)
{
	_x = x;
	_y = y;
	_th = th;
}

void Pose::setPose(float _x, float _y, float _th)
{
	setX(_x);
	setY(_y);
	setTh(_th);
}

float Pose::findDistanceTo(Pose pos)
{
	return 0.0f;
}

float Pose::findAngleTo(Pose pos)
{
	return 0.0f;
}
