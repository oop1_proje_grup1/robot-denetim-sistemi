#pragma once

/**
*\file Path.h
*\author Alper DEMIREL
*\date 20.12.2018
*\brief Path sinifi ve fonksiyonlarinin prototipleri
*/

#include"Node.h"
#include<iostream>

using namespace std;

/**
*\brief Linked list formunda yol planini yonetmeyi saglayan sinif
*/
class Path
{
private:

	/**
	*\brief Listenin kuyrugunu gosteren pointer
	*/
	Node* tail;

	/**
	*\brief Listenin basini gosteren pointer
	*/
	Node* head;


	/**
	*\brief Liste numarasini gosteren deger
	*/
	int number;

public:

	/**
	*\brief Varsayilan yapici fonksiyon
	*/
	Path();

	/**
	*\brief Varsayilan yikici fonksiyon
	*/
	~Path();

	/**
	*\brief Pozisyonun konumunu listenin sonuna ekleyen fonksiyon
	*\param pos: Listenin sonuna eklenecek pozisyon
	*/
	void addPos(Pose pos);

	/**
	*\brief Listede bulunan pozisyonlar� ekrana yazdira fonksiyon
	*\param pos: Listenin ba��n� temsil eden de�er
	*/
	void print(Node* head);

	/**
	*\brief Verilen indeksteki konumu dondurecek olan operatorun fonksiyonu
	*\param i: Verilen indeksin degeri
	*\return Verilen indeksteki konum
	*/
	int  operator[](int i);

	/**
	*\brief Verilen indeksteki konumu d�nd�ren fonksiyon
	*\param index: Verilen indeksin degeri
	*\return Verilen indeksteki konum
	*/
	Pose getPos(int index);

	/**
	*\brief Verilen indeksteki konumu listeden silen fonksiyon
	*\param index: Verilen indeksin degeri
	*\return Verilen konum silindiyse true silinmediyse false 
	*/
	bool removePos(int index);

	/**
	*\brief Verilen indeksten bir sonraya konum ekleyen fonksiyon
	*\param index: Verilen indeksin degeri
	*\param pose: Eklenen yeni pozisyonun(konum) degeri
	*\return Yeni eklenen pozisyon varsa true yoksa false
	*/
	bool insertPos(int index, Pose pose);

	/**
	*\brief print fonksiyonunun yaptigi islevi gormesi icin saglanan operat�r�n fonksiyonu
	*\param Pose: Islem yapacagimiz pozisyon
	*\return print isleminin sonucu
	*/
	Pose & operator<<(Pose*pose);

	/**
	*\brief klavyeden girilen konum icin saglanan operat�r�n fonksiyonu
	*\param Pose: Islem yapacagimiz pozisyon
	*\return Klavyeden girilen pozisyon
	*/
	Pose & operator>>(Pose* pose);
};

