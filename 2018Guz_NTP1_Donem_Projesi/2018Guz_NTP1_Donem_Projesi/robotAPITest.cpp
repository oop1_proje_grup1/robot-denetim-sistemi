#include "PioneerRobotAPI.h"
#include"RobotControl.h"
#include <iostream>
using namespace std;

/**
* \file robotAPITest.cpp
* \author Alper DEMIREL
* \date 20.12.2018
* \brief Uygulamayi test etmek icin yazilmis fonksiyonlar
*/

/**
* \brief connectionMenu, robot simulatoruyle baglantiyi acmak ve kapatmak icin kullanilan menu fonksiyonu.
* \param PionerRobotAPI (Robot simulatorune baglanacak parametre)
*/
void connectionMenu(PioneerRobotAPI& robotAPI) {

	int choice;

	const int CONNECT_CHOICE = 1,
		DISCONNECT_CHOICE = 2,
		BACK_CHOICE = 3;

	do {

		system("cls");
		cout << "+-----------------+" << endl
			<< "| CONNECTION MENU |" << endl
			<< "+-----------------+" << endl
			<< "1. Connect Robot" << endl
			<< "2. Disconnect Robot" << endl
			<< "3. Back" << endl
			<< "Choose one : ";
		cin >> choice;

		while (choice < CONNECT_CHOICE || choice > BACK_CHOICE) {

			cout << "Please enter a valid menu choice: ";
			cin >> choice;
		}

		if (choice != BACK_CHOICE) {

			switch (choice) {

			case CONNECT_CHOICE:

				if (!robotAPI.connect())
					cout << "Robot is not connected..." << endl;
				else
					cout << "Robot is connected..." << endl;

				Sleep(1000);
				break;

			case DISCONNECT_CHOICE:

				if (!robotAPI.disconnect())
					cout << "Robot is disconnected..." << endl;
				else
					cout << "Robot is not disconnected..." << endl;

				Sleep(1000);
				break;
			}

		}

	} while (choice != BACK_CHOICE);
}

/**
* \brief motionMenu, robotun hareketlerini duzenlemek icin kullanilan menu fonksiyonu.
* \param PionerRobotAPI (Hareketi yonlendirilecek olan robot parametresi)
* \param robotControl (Robotun hareketlerini ayarlamak icin kullanilan parametre)
*/
void motionMenu(PioneerRobotAPI& robotAPI, RobotControl& robotControl) {

	int choice;

	const int MOVEROBOT_CHOICE = 1,
		STOPROBOT_CHOICE = 2,
		TURNLEFT_CHOICE = 3,
		TURNRIGHT_CHOICE = 4,
		FORWARD_CHOICE = 5,
		BACKWARD_CHOICE = 6,
		STOPTURN_CHOICE = 7,
		STOPMOVE_CHOICE = 8,
		QUIT_CHOICE = 9;

	do {
		system("cls");
		cout << "+-------------+" << endl
			<< "| MOTION MENU |" << endl
			<< "+-------------+" << endl
			<< "1. Move robot" << endl
			<< "2. Stop robot" << endl
			<< "3. Turn left" << endl
			<< "4. Turn right" << endl
			<< "5. Forward" << endl
			<< "6. Backward" << endl
			<< "7. Stop turn" << endl
			<< "8. Stop move" << endl
			<< "9. Quit" << endl
			<< "Choose one : ";
		cin >> choice;

		while (choice < MOVEROBOT_CHOICE || choice > QUIT_CHOICE) {

			cout << "Please enter a valid menu choice: ";
			cin >> choice;
		}

		if (choice != QUIT_CHOICE) {

			switch (choice) {

				float speed;
				float distance;

			case MOVEROBOT_CHOICE:
				cout << "Enter speed: ";
				cin >> speed;

				robotAPI.moveRobot(speed);
				break;

			case STOPROBOT_CHOICE:
				robotAPI.stopRobot();
				break;

			case TURNLEFT_CHOICE:
				robotControl.turnLeft();
				break;

			case TURNRIGHT_CHOICE:
				robotControl.turnRight();
				break;

			case FORWARD_CHOICE:
				cout << "Enter speed: ";
				cin >> speed;
				robotControl.forward(speed);
				break;

			case BACKWARD_CHOICE:
				cout << "Enter speed: ";
				cin >> speed;
				robotControl.backward(speed);
				break;

			case STOPTURN_CHOICE:
				robotControl.stopTurn();
				break;

			case STOPMOVE_CHOICE:
				robotControl.stopMove();
				break;
			}
		}

	} while (choice != QUIT_CHOICE);
}

/**
* \brief showMenu, robotun baglantisini ve hareketlerini ayarlamak icin kullanilan menu fonksiyonu.
* \param PioneerRobotAPI (Hareketleri ve baglantisi ayarlanacak olan robot parametresi)
* \param robotControl (Robotun hareketlerini kontrol etmek icin kullanilan parametre)
*/
void showMenu(PioneerRobotAPI& robotAPI, RobotControl& robotControl) {

	int choice;

	const int CONNECTION_CHOICE = 1,
		MOTION_CHOICE = 2,
		QUIT_CHOICE = 3;

	do {

		system("cls");
		cout << "+-----------+" << endl
			<< "| MAIN MENU |" << endl
			<< "+-----------+" << endl
			<< "1. Connection" << endl
			<< "2. Motion" << endl
			<< "3. Quit" << endl
			<< "Choose one : ";
		cin >> choice;

		while (choice < CONNECTION_CHOICE || choice > QUIT_CHOICE) {

			cout << "Please enter a valid menu choice: ";
			cin >> choice;
		}

		if (choice != QUIT_CHOICE) {

			switch (choice) {

			case CONNECTION_CHOICE:
				connectionMenu(robotAPI);
				break;

			case MOTION_CHOICE:
				motionMenu(robotAPI, robotControl);
				break;
			}
		}

	} while (choice != QUIT_CHOICE);
}



PioneerRobotAPI *robot;
float sonars[16];
float laserData[181];

void print() {
	cout << "MyPose is (" << robot->getX() << "," << robot->getY() << "," << robot->getTh() << ")" << endl;
	cout << "Sonar ranges are [ ";
	robot->getSonarRange(sonars);
	for (int i = 0; i < 16; i++) {
		cout << sonars[i] << " ";
	}
	cout << "]" << endl;
	cout << "Laser ranges are [ ";
	robot->getLaserRange(laserData);
	for (int i = 0; i < 181; i++) {
		cout << laserData[i] << " ";
	}
	cout << "]" << endl;
}

/**
* \brief Main fonksiyonu
*/
int main() {

	robot = new PioneerRobotAPI;

	if (!robot->connect()) {
		cout << "Could not connect..." << endl;
		return 0;
	}

	robot->moveRobot(100);
	print();

	robot->turnRobot(PioneerRobotAPI::DIRECTION::left);
	Sleep(1000);
	print();

	robot->turnRobot(PioneerRobotAPI::DIRECTION::forward);
	Sleep(1000);
	print();

	robot->turnRobot(PioneerRobotAPI::DIRECTION::right);
	Sleep(1000);
	print();

	robot->stopRobot();
	robot->setPose(100, 200, 30);
	Sleep(1000);
	print();

	cout << "Press any key to exit...";
	getchar();

	robot->disconnect();
	delete robot;
	return 0;

}