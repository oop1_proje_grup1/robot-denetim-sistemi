#include "SonarSensor.h"

/**
* @file SonarSensor.cpp
* @author Mehmet Yumrukaya
* @date 20.12.2018
* @brief SonarSensor sinifinin kodlamalari
*/


SonarSensor::SonarSensor()
{
}

SonarSensor::~SonarSensor()
{
}

float SonarSensor::getRange(int index) {

	return ranges[index];
}

float SonarSensor::getMax() {

	float max = ranges[0];

	for (int i = 0; i < 16; i++) {
		if (max < ranges[i])
			max = ranges[i];
	}
	return max;
}

float SonarSensor::getMin() {

	float min = ranges[0];

	for (int i = 0; i < 16; i++) {
		if (min < ranges[i])
			min = ranges[i];
	}
	return min;
}

void SonarSensor::updateSensor(float ranges[]) {

	for (int i = 0; i < 16; i++) {
		this->ranges[i] = ranges[i];
	}
}

float SonarSensor::operator[](int i) {

	return ranges[i];
}

