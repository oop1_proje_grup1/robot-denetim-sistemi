#pragma once
#include "PioneerRobotAPI.h"

/**
* @file SonarSensor.h
* @author Mehmet Yumrukaya
* @date 20.12.2018
* @brief SonarSensor sinifi ve fonksiyonlarinin prototipleri
*/

/**
*\brief Robotun mesafe bilgilerini olcmek ve ayarlamak icin kullanilan siniftir
*/
class SonarSensor
{
private:
	/*
	* \brief Mesafe bilgilerini tutan dizi.
	*/
	float ranges[16];

	/*
	* \brief Gerekli olan parametreleri main siniftan header dosyasina alan pointer
	*/
	PioneerRobotAPI* robotAPI;

public:
	/*
	* \brief Yapici fonksiyon.
	*/
	SonarSensor();

	/*
	* \brief Yikici fonksiyon.
	*/
	~SonarSensor();

	/*
	* \brief Parametre olarak gelen numaradaki sensorun olctugu mesafe bilgisini dondurur.
	* \param index : olculecek sensor numarasi.
	* \return sensorun mesafe bilgisi
	*/
	float getRange(int index);

	/*
	* \brief Sensorun olctugu maksimum mesafeyi isler.
	* \return maksimum sensor mesafesi
	*/
	float getMax();

	/*
	* \brief Sensorun olctugu minimum mesafeyi dondurur.
	* \return minimum sensor mesafesi
	*/
	float getMin();

	/*
	* \brief Kullanicinin girdigi bilgilerle, sensorun mesafe bilgilerini gunceller.
	* \param ranges : guncel bilgiler.
	*/
	void updateSensor(float ranges[]);

	/*
	* \brief Kullanicidan aldigi indeks numarasina karsilik gelen sensorun mesafe bilgilerini dondurur.
	* \param i : indeks numarasi
	* \return sensor bilgisi
	*/
	float operator[] (int i);
};

