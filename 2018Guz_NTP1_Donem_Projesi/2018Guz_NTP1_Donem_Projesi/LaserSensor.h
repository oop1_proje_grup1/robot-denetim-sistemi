#pragma once
#include"PioneerRobotAPI.h"

/**
* @file LaserSensor.h
* @author Mehmet Yumrukaya
* @date 20.12.2017
* @brief LaserSensor sinifi ve fonksiyonlarinin prototipleri
*/

/**
* \brief Robotun mesafe bilgilerini olcmek ve ayarlamak icin kullanilan siniftir
*/

class LaserSensor
{
private:
	/*
	* \brief Mesafe bilgilerini tutan dizi.
	*/
	float ranges[181];

	/*
	* \brief Gerekli olan parametreleri main siniftan header dosyasina alan pointer
	*/
	PioneerRobotAPI* robotAPI;

public:
	/*
	* \brief Yapici fonksiyon.
	*/
	LaserSensor();

	/*
	* \brief Yikici fonksiyon.
	*/
	~LaserSensor();

	/*
	* \brief Parametre olarak gelen numaradaki sensorun olctugu mesafe bilgisini dondurur.
	* \param index : olculecek sensor numarasi.
	* \return sensorun mesafe bilgisi
	*/
	float getRange(int index);

	/*
	* \brief Kullanicinin girdigi bilgilerle, sensorun mesafe bilgilerini gunceller.
	* \param ranges : guncel bilgiler.
	*/
	void updateSensor(float ranges[]);

	/*
	* \brief Sensorun olctugu maksimum mesafeyi isler.
	* \return maksimum sensor mesafesi
	*/
	float getMax();

	/*
	* \brief Sensorun olctugu minimum mesafeyi dondurur.
	* \return minimum sensor mesafesi
	*/
	float getMin();

	/*
	* \brief Kullanicidan aldigi indeks numarasina karsilik gelen sensorun mesafe bilgilerini dondurur.
	* \param i : indeks numarasi
	* \return sensor bilgisi
	*/
	float operator[] (int i);
};


