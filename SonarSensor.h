#pragma once
#include "PioonerRoborAPI.h"

/*-> File : SonarSensor.h
  -> Author : Mehmet Yumrukaya
  -> Date : 20.12.2018
  */

class SonarSensor  
{
private:
	
	float ranges[16];

	PioneerRobotAPI* robotAPI;

public:
	SonarSensor();

	~SonarSensor();

	float getRange(int index);
	float getMax();
	float getMin();
	void updateSensor(float ranges[]);
	float operator[] (int i);
};

